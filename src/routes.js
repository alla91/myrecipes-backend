import Boom from 'boom'
import db from './server'

const routes = [
    {
        method: 'GET',
        path:'/all',
        handler: function (request, reply) {
            db.recipes.find((err, recipes) => {
                if(err) {
                    return reply(Boom.wrap(err, 'Internal MongoDB error'))
                }
                return reply({recipes})
            })
        }
    },
    {
        method: 'GET',
        path:'/recipe/{id}',
        handler: function (request, reply) {
            db.recipes.findOne({
                _id: request.params.id
            },(err, recipe) => {
                if(err) {
                    return reply(Boom.wrap(err, 'Internal MongoDB error'))
                }

                if(!recipe) {
                    return reply(Boom.notFound('Recipe not found'))
                }

                return reply(recipe)
            })
        }
    }
]

export default routes