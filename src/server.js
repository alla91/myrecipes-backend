import Hapi from 'hapi'
import routes from './routes'
import mongojs from 'mongojs'

const server = new Hapi.Server()
const port = Number(process.env.PORT || 3000)

const connectionProperties = {
    port: port,
    routes: {
        cors: true
    },
    labels: ['app']
}

server.connection(connectionProperties)

const app = server.select('app')
app.db = mongojs(process.env.DATABASE_URL, ['recipes'])


routes.forEach((route) => {
    console.log(`Attaching ${route.method} ${route.path}`)
    server.route(route)
})


server.start(err => {
    if (err) {
        throw err
    }
    console.log('Server running at:', server.info.uri)
})

export default app.db