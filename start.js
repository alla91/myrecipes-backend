// register babel-core
require('babel-core/register')
// set env config
require('env2')('.env')
// start server
require('./src/server')
